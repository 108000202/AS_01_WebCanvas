const canvas = document.getElementById('my-canvas');
const ctx = canvas.getContext('2d');
ctx.lineJoin = 'round';
ctx.lineCap = 'round';
ctx.lineWidth = 15;
//record which tool is using currently
let currentState = 'brush'
//record the current color using by brush tool
let currentColor = 'black';
//record the current color using by rainbow pen tool
let rainbowColor = 0;
//record the current text font using by text input tool
let currentFont = 'serif';
//record the current text size using by text input tool
let currentSize = '10';
//record whether the mouse is pressed when drawing
let isDrawing = false;
//record whether the mouse is pressed when dragging shape
let isDragging = false;
//record the mouse's position
let [lastX, lastY] = [0, 0];
//record the last state of canvavs
let step = -1;
let historyArray = new Array();
//record the slider bar
var slider = document.getElementById("myRange");
//record the color selected
var color = document.getElementById("myColor");
//record the text font and size selected
//var font = document.getElementById("font");
//var size = document.getElementById("size");
//record the text input state
var inputted = false;
//record the canvas before dragged
var tempCanvas = new Image();
/*initialize*/
push();
canvas.style.cursor = "url('source/brush_c.png'), auto";


/*handling mouse event*/
canvas.addEventListener('mousedown', (event) => {
    if(currentState === 'text') {
        if(inputted) return;
        console.log(event.offsetX);
        addInputbox(event.offsetX, event.offsetY);
    }
    else if(currentState === 'line' || currentState === 'rectangle' || currentState === 'circle' || currentState === 'triangle') {
        isDragging = true;
        isDrawing = true;
        [startX, startY] = [event.offsetX, event.offsetY];
        tempCanvas.src = historyArray[step];
    }
    else {
        isDrawing = true;
        [lastX, lastY] = [event.offsetX, event.offsetY];
    }
});
canvas.addEventListener('mousemove', (event) => {
    if(!isDrawing) return;
    if(currentState === 'brush' || currentState === 'rainbow') {
        if(currentState === 'brush') {
            ctx.strokeStyle = currentColor;
        }
        else {
            ctx.strokeStyle = `hsl(${rainbowColor}, 100%, 50%)`;
            rainbowColor = (rainbowColor + 1) % 360;
        }
        ctx.beginPath();
        ctx.moveTo(lastX, lastY);
        ctx.lineTo(event.offsetX, event.offsetY);
        ctx.stroke();
        [lastX, lastY] = [event.offsetX, event.offsetY];
    }
    else if(currentState === 'eraser') {
        ctx.clearRect(lastX, lastY, ctx.lineWidth, ctx.lineWidth);
        [lastX, lastY] = [event.offsetX, event.offsetY];
    }
    else if(currentState === 'line' || currentState === 'rectangle' || currentState === 'circle' || currentState === 'triangle') {
        if(!isDragging) return;
        ctx.strokeStyle = currentColor;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(tempCanvas, 0, 0);
        if(currentState === 'line') {
            ctx.beginPath();
            ctx.moveTo(startX, startY);
            ctx.lineTo(event.offsetX, event.offsetY);
            ctx.stroke();
        }
        else if(currentState === 'rectangle') {
            var [width, height] = [event.offsetX-startX, event.offsetY-startY];
            ctx.strokeRect(startX, startY, width, height);
        }
        else if(currentState === 'triangle') {
            ctx.beginPath();
            ctx.moveTo(startX, startY);
            ctx.lineTo(event.offsetX, event.offsetY);
            ctx.lineTo(Math.abs(event.offsetX-2*(event.offsetX-startX)), event.offsetY);
            ctx.closePath();
            ctx.stroke();
        }
        else if(currentState === 'circle') {
            ctx.save();
            var a = Math.abs(event.offsetX-startX);
            var b = Math.abs(event.offsetY-startY);
            var r = (a > b)? a : b;
            var ratioX = a / r;
            var ratioY = b / r;
            var centerX = Math.abs(event.offsetX-(event.offsetX-startX)/2);
            var centerY = Math.abs(event.offsetY-(event.offsetY-startY)/2);
            ctx.scale(ratioX, ratioY);
            ctx.beginPath();
            ctx.arc(centerX / ratioX, centerY / ratioY, r, 0, 2 * Math.PI, false);
            ctx.closePath();
            ctx.stroke();
            ctx.restore();
        }
    }
    else if (currentState === 'watercolor') {
        var radius = (Math.random() * ctx.lineWidth) % ctx.lineWidth;
        var opacity = Math.random();
        ctx.save();
        ctx.fillStyle = currentColor;
        ctx.beginPath();
        ctx.globalAlpha = opacity;
        ctx.arc(lastX, lastY, radius, false, Math.PI * 2, false);
        ctx.fill();
        [lastX, lastY] = [event.offsetX, event.offsetY];
        ctx.restore();
    }
    else if (currentState === 'pencil') {
        ctx.save();
        ctx.beginPath();
        ctx.strokeStyle = currentColor;
        var currentWidth = ctx.lineWidth;
        ctx.lineWidth = Math.floor(Math.random() * 7) + (currentWidth - 3);
        ctx.moveTo(lastX, lastY);
        ctx.lineTo(event.offsetX, event.offsetY);
        ctx.stroke();
        [lastX, lastY] = [event.offsetX, event.offsetY];
        ctx.restore();
    }
});	
canvas.addEventListener('mouseup', () => {
    if(isDrawing || isDragging) push();
    isDragging = false;
    isDrawing = false;

});
canvas.addEventListener('mouseout', () => {
    if(isDrawing || isDragging) push();
    isDragging = false;
    isDrawing = false;
});

/*to save the current canvas as data in an array*/
function push() {
    step++;
    if (step < historyArray.length) {
        historyArray.length = step;
    }
    console.log(step);
    historyArray.push(canvas.toDataURL());
}

/*to change brush linewidth*/
slider.oninput = function() {
    ctx.lineWidth = this.value;
}

/*to change color*/
color.oninput = function() {
    console.log(this.value);
    currentColor = this.value;
}
/*
to change font and size
font.oninput = function() {
    console.log(this.value);
    currentFont = this.value;
}
size.oninput = function() {
    console.log(this.value);
    currentSize = this.value;
}
*/
/*Basic*/
/*handling the basic tools*/
var brush_clicked = function() {
    currentState = 'brush';
    canvas.style.cursor = "url('source/brush_c.png'), auto";
}

var eraser_clicked = function() {
    currentState = 'eraser';
    canvas.style.cursor = "url('source/eraser_c.png'), auto";
}

/*handling the basic shape drawing*/
var line_clicked = function() {
    currentState = 'line';
    canvas.style.cursor = "url('source/line_c.png'), auto";
}
var rectangle_clicked = function() {
    currentState = 'rectangle';
    canvas.style.cursor = "url('source/rectangle_c.png'), auto";
}
var circle_clicked = function() {
    currentState = 'circle';
    canvas.style.cursor = "url('source/circle_c.png'), auto";
}
var triangle_clicked = function() {
    currentState = 'triangle';
    canvas.style.cursor = "url('source/triangle_c.png'), auto";
}

/*handling the undo/redo tools*/
var undo_clicked = function() {
    if(step > 0) {
        step--;
        console.log(step);
        var lastCanvas = new Image();
        lastCanvas.src = historyArray[step];
        lastCanvas.onload = function() {
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(lastCanvas, 0, 0);
        }
    }
}
var redo_clicked = function() {
    if(step < historyArray.length - 1) {
        step++;
        console.log(step);
        var nextCanvas = new Image();
        nextCanvas.src = historyArray[step];
        nextCanvas.onload = function() {
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(nextCanvas, 0, 0);
        }
    }
}

/*handling text input*/
var text_clicked = function() {
    currentState = 'text';
    canvas.style.cursor = "text";
}
var addInputbox = function(x, y) {
    var input = document.createElement('input');
    input.type = 'text';
    input.style.position = 'fixed';
    input.style.left = x + 50 + 'px';//should add the margin of the canvas
    input.style.top = y + 50 + 'px';
    input.onkeydown = function(key) {
        var keyName = key.key;
        if(keyName === "Enter") {
            ctx.textBaseline = 'top';
            ctx.textAlign = 'left';
            ctx.fillStyle = currentColor;
            ctx.font = currentSize + "pt " + currentFont;
            ctx.fillText(input.value, x, y);        
            document.body.removeChild(input);
            inputted = false;
            push();
        }
    };
    document.body.appendChild(input);
    input.focus();
    inputted = true;
}

/*handling the clean tool*/
var clean_clicked = function() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

/*handling the image download tool*/
var download_clicked = function() {
    var data = canvas.toDataURL("image/png");
    var link = document.createElement('a');
    link.download = "download.png";
    link.href = data;
    link.click();
}

/*handling image upload and paste*/
var image_clicked = function() {
    var inputObj = document.createElement('input');
    inputObj.addEventListener('change', readFile, false);
    inputObj.type = 'file';
    inputObj.accept = 'image/*';
    inputObj.id = 'btn';
    inputObj.click();
}
function readFile() {
    var file = this.files[0];
    if(file.type === 'image/jpeg' || file.type === 'image/png'){
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function(e){
            drawToCanvas(this.result);
        }
    }
    else {
        alert("Unsupported file type!");
        return false;
    }
}
function drawToCanvas(imgData){
    var img = new Image;
    img.src = imgData;
    img.onload = function(){//必須onload之後再畫
        ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
        push();
    }
}

/*Advanced*/
var pencil_clicked = function() {
    //todo
    currentState = 'pencil';
    canvas.style.cursor = "url('source/pencil_c.png'), auto";
}

var watercolor_clicked = function() {
    currentState = 'watercolor';
    canvas.style.cursor = "url('source/watercolor_c.png'), auto";
}

var rainbow_clicked = function() {
    currentState = 'rainbow';
    canvas.style.cursor = "url('source/rainbow_c.png'), auto";
}


/*Appearence of select box*/
var x, i, j, l, ll, selElmnt, a, b, c;
/* Look for any elements with the class "custom-select": */
x = document.getElementsByClassName("custom-select");
l = x.length;
for (i = 0; i < l; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  ll = selElmnt.length;
  /* For each element, create a new DIV that will act as the selected item: */
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /* For each element, create a new DIV that will contain the option list: */
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 0; j < ll; j++) {
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /* When an item is clicked, update the original select box,
        and the selected item: */
        var y, i, k, s, h, sl, yl;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        sl = s.length;
        h = this.parentNode.previousSibling;
        for (i = 0; i < sl; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            /*to change font and size*/
            let content = this.innerHTML;
            if(!isNaN(Number(content))) {
                currentSize = content;
            }
            else {
                currentFont = content;
            }
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            yl = y.length;
            for (k = 0; k < yl; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
    /* When the select box is clicked, close any other select boxes,
    and open/close the current select box: */
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle("select-hide");
    this.classList.toggle("select-arrow-active");
  });
}

function closeAllSelect(elmnt) {
  /* A function that will close all select boxes in the document,
  except the current select box: */
  var x, y, i, xl, yl, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  xl = x.length;
  yl = y.length;
  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}

/* If the user clicks anywhere outside the select box,
then close all select boxes: */
document.addEventListener("click", closeAllSelect);

/*keyboard control*/
document.addEventListener('keydown', (event) => {
    const keyName = event.key;
  
    if (keyName === 'Control') {
      return;
    }
  
    if (event.ctrlKey) {
        if(keyName === 'z') {
            undo_clicked();
        }
        else if(keyName === 'y') {
            redo_clicked();
        }
        else if(keyName === 's') {
            download_clicked();
        }
        else if(keyName === 'v') {
            image_clicked();
        }
    }
  }, false);