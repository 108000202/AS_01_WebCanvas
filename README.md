# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Rainbow color brush                              | 1~5%     | Y         |
| Watercolor brush                                 |--      | Y         |
| Pencil                                           |--      | Y         |
| Keyboard Control                                  |--      | Y         |



---

### How to use 
![APPEARENCE](view.jpg)

    The user can use the mouse to draw something in the white area.  
    By clicking the buttons on the right, there are multiple tools provided, we will introduce them respectively in the next part.

### Function description

##### Text font/size
    Change the font and size of the text input.

##### Color selector
    Change the color of the brush and the text input.

##### Brush Size
    Change the width of the brush.

##### Brush
    Just a normal brush.

##### Eraser
    Clear the selected area by a square with width equals to the brush's width. 

##### Ruler
    Draw a line from the point that the mouse was clicked to the position of the cursor.

##### Rectangle drawing tool
    Draw a rectangle from the point that the mouse was clicked to the position of the cursor.


##### Oval/Circle drawing tool
    Draw an oval/circle from the point that the mouse was clicked to the position of the cursor.


##### Triangle drawing tool
    Draw a triangle from the point that the mouse was clicked to the position of the cursor.


##### Undo/Redo
    Record the canvas as an image each time the user add new element, then use it to undo the last move or redo the next move.

##### Text input
    When the mouse is clicked in the canvas, the user can type something in the input box.
    Once Enter is pressed, the text will appear in the canvas with certain text font and size.

##### Clean
    Clean all the content in the canvas.

##### Download
    Export the canvas as a PNG file.

##### Upload Image
    Import a PNG/JPEG image file from the device.

##### Rainbow color brush
    Constantly change the color in the order of rainbow colors while drawing.

##### Watercolor brush
    Generate circles following the mouse's locus with random size and transparency to simulate watercolor paintings or ink paintings style.

##### Pencil
    Randomly change the brush's width while drawing to simulate the handwriting style.

##### Keyboard Control
| **Keyname**                         | **Function** |
| :----------------------------------------------- | :-------: |
| Ctrl+Z                              | Undo     |
| Ctrl+Y                                 |Redo      |
| Ctrl+S                                           |Save      |
| Ctrl+V                                  |Upload Image      |

### Gitlab page link

<https://108000202.gitlab.io/AS_01_WebCanvas>

